package com.example.QualitasCore

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class QualitasCoreApplication



fun main(args: Array<String>) {
	runApplication<QualitasCoreApplication>(*args)

}
