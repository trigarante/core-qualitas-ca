package com.example.QualitasCore.Model

import com.fasterxml.jackson.annotation.JsonProperty

class RequestWs (
        @get:JsonProperty("aseguradora", required=true)@field:JsonProperty("aseguradora", required=true)
        val aseguradora: String,

        @get:JsonProperty("clave", required=true)@field:JsonProperty("clave", required=true)
        val clave: String,

        @get:JsonProperty("cp", required=true)@field:JsonProperty("cp", required=true)
        val cp: String,

        @get:JsonProperty("descripcion", required=true)@field:JsonProperty("descripcion", required=true)
        val descripcion: String,

        @get:JsonProperty("descuento", required=true)@field:JsonProperty("descuento", required=true)
        val descuento: Long,

        @get:JsonProperty("edad", required=true)@field:JsonProperty("edad", required=true)
        val edad: String,

        @get:JsonProperty("fechaNacimiento", required=true)@field:JsonProperty("fechaNacimiento", required=true)
        val fechaNacimiento: String,

        @get:JsonProperty("genero", required=true)@field:JsonProperty("genero", required=true)
        val genero: String,

        @get:JsonProperty("marca", required=true)@field:JsonProperty("marca", required=true)
        val marca: String,

        @get:JsonProperty("modelo", required=true)@field:JsonProperty("modelo", required=true)
        val modelo: String,

        @get:JsonProperty("movimiento", required=true)@field:JsonProperty("movimiento", required=true)
        val movimiento: String,

        @get:JsonProperty("paquete", required=true)@field:JsonProperty("paquete", required=true)
        val paquete: String,

        @get:JsonProperty("servicio", required=true)@field:JsonProperty("servicio", required=true)
        val servicio: String
        ){

}