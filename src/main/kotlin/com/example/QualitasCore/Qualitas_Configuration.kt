package com.example.QualitasCore

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.oxm.jaxb.Jaxb2Marshaller

@Configuration
class Qualitas_Configuration {

    @Bean
    fun marshaller() : Jaxb2Marshaller{
        val marshal = Jaxb2Marshaller()
        marshal.contextPath = "core.qualitas.wsdl"
        return  marshal
    }

    @Bean
    fun qualitasClient (marshaller: Jaxb2Marshaller?) : Qualitas_Controller{
        val client : Qualitas_Controller = Qualitas_Controller()
        client.defaultUri = "http://sio.qualitas.com.mx/WsEmision/WsEmision.asmx"
        client.marshaller = marshaller
        client.unmarshaller = marshaller
        return client
    }
}