package com.example.QualitasCore.Controller.CotizacionEmision

import com.example.QualitasCore.Model.RequestWs
import com.example.QualitasCore.Model.Seguro
import com.example.QualitasCore.Qualitas_Controller
import com.example.QualitasCore.Service.CotizacionEmision.Cotizacion_Emision_Autos_Service
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("autos")
class Cotizacion_Emision_Autos_Controller {
    @Autowired
    lateinit var qualitasAutosService : Cotizacion_Emision_Autos_Service
    @Autowired
    lateinit var  controladorQualitas : Qualitas_Controller

    @GetMapping("/cotizaciones")
    fun getCotizacion() : ResponseEntity<Any> {

        var cotizacion = controladorQualitas.cotizadorQualitas(qualitasAutosService.getXML())
        return  ResponseEntity(cotizacion, HttpStatus.OK)
    }
}