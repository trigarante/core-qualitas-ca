package com.example.QualitasCore.Service.CotizacionEmision

import com.example.QualitasCore.Model.*
import com.example.QualitasCore.Qualitas_Controller
import org.json.JSONObject
import org.json.XML
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.stereotype.Repository
import org.springframework.web.client.RestTemplate
import java.time.Duration
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter

@Repository
class Cotizacion_Emision_Autos_Service {
    @Autowired
    lateinit var  controladorQualitas : Qualitas_Controller

    var negocio = "2886"
    var fechaInicio = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE)
    var fechaFin = LocalDateTime.now().plusYears(1).format(DateTimeFormatter.ISO_DATE)
    var agente = "71315"
    var xmlCotizacion : String = ""
    var urlLog : String = "http://core-logs-ws-dev.us-east-1.elasticbeanstalk.com/log_coches/cotizaciones"
    var descuento : String  = "30"


    /*fun cotizacion(request : RequestWs, seguro: Seguro) : Seguro{

           var jsonEntrada = JSONObject(request).toString()
            var timerStart = LocalTime.now() // capturador de tiempo
            var clave = request.clave
            var direcciones = direccionesQualitas.getDireccionesByCPostal(request.cp)
            xmlCotizacion = getXML(direcciones[0].IDEdo_Qua, request.cp, request)
            var timerFinishRequest = LocalTime.now() // fin de captura de tiempo
            var duracion: Duration = Duration.between(timerStart, timerFinishRequest) // calculo de tiempo transcurrido
            var timeRequest: String = duracion.seconds.toString() + "." + duracion.nano.toString().substring(0, 3)

            var soapResponse = controladorQualitas.cotizadorQualitas(xmlCotizacion)// Respuesta de el servicio SOAP

            var timerFinishResponse = LocalTime.now()
            var duracionResponse: Duration = Duration.between(timerStart, timerFinishResponse)
            var timerResponse: String = duracionResponse.seconds.toString() + "." + duracionResponse.nano.toString().substring(0, 3)

            var jsonObj = XML.toJSONObject(soapResponse.obtenerNuevaEmisionResult)
            var movimientos = jsonObj.getJSONObject("Movimientos")
            var movi = movimientos.getJSONObject("Movimiento")
            var codigoError = movi.getString("CodigoError")

            if (codigoError != "") {
                seguro.codigoError = codigoError
                seguro.Aseguradora = "QUALITAS"
                seguro.vehiculo.clave = request.clave
                seguro.vehiculo.descripcion = request.descripcion
                seguro.vehiculo.modelo = request.modelo
                seguro.vehiculo.marca = request.marca
                seguro.cliente.direccion.codPostal = request.cp
                seguro.paquete = request.paquete
                seguro.descuento = request.descuento.toString()
                seguro.cotizacion.resultado = "False"
                return seguro
            } else {

                //Extraccion de coberturas
                var datosVehiculo = movi.getJSONObject("DatosVehiculo")
                var jsonCoberturas = datosVehiculo.getJSONArray("Coberturas")
                var coberturas: MutableList<Cobertura> = mutableListOf()
                for (cob in 0 until jsonCoberturas.length()) {
                    if (jsonCoberturas.getJSONObject(cob).getInt("NoCobertura") == 1) {
                        var nombre = "Daños Materiales"
                        var sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada")
                        var deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible")
                        coberturas.add(Cobertura(nombre, sumaAsegurada.toString(), deducible.toString()))

                    } else if (jsonCoberturas.getJSONObject(cob).getInt("NoCobertura") == 3) {
                        var nombre = "Robo Total"
                        var sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada")
                        var deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible")
                        coberturas.add(Cobertura(nombre, sumaAsegurada.toString(), deducible.toString()))

                    } else if (jsonCoberturas.getJSONObject(cob).getInt("NoCobertura") == 4) {
                        var nombre = "Responsabilidad Civil"
                        var sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada")
                        var deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible")
                        coberturas.add(Cobertura(nombre, sumaAsegurada.toString(), deducible.toString()))

                    } else if (jsonCoberturas.getJSONObject(cob).getInt("NoCobertura") == 5) {
                        var nombre = "Gastos Medicos"
                        var sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada")
                        var deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible")
                        coberturas.add(Cobertura(nombre, sumaAsegurada.toString(), deducible.toString()))

                    } else if (jsonCoberturas.getJSONObject(cob).getInt("NoCobertura") == 7) {
                        var nombre = "Gastos Legales"
                        var sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada")
                        var deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible")
                        coberturas.add(Cobertura(nombre, sumaAsegurada.toString(), deducible.toString()))

                    } else if (jsonCoberturas.getJSONObject(cob).getInt("NoCobertura") == 6) {
                        var nombre = "Accidentes ocupantes"
                        var sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada")
                        var deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible")
                        coberturas.add(Cobertura(nombre, sumaAsegurada.toString(), deducible.toString()))
                    }
                }
                var listaCoberturas: MutableList<Cobertura> = coberturas
                seguro.coberturas = listaCoberturas
                seguro.cotizacion.resultado = "true"
                seguro.cotizacion.primaNeta = movi.getJSONObject("Primas").getDouble("PrimaNeta").toString()
                seguro.cotizacion.derechos = movi.getJSONObject("Primas").getDouble("Derecho").toString()
                seguro.cotizacion.primaTotal = movi.getJSONObject("Primas").getDouble("PrimaTotal").toString()
                seguro.cotizacion.impuesto = movi.getJSONObject("Primas").getDouble("Impuesto").toString()
                seguro.Aseguradora = "QUALITAS"
                seguro.vehiculo.clave = request.clave
                seguro.vehiculo.descripcion = request.descripcion
                seguro.vehiculo.modelo = request.modelo
                seguro.vehiculo.marca = request.marca
                seguro.cliente.direccion.codPostal = request.cp
                seguro.paquete = request.paquete
                seguro.cotizacion.idCotizacion = movi.getString("NoCotizacion")

                var timeFinishTotal = LocalTime.now()
                var duracionTotal: Duration = Duration.between(timerStart, timeFinishTotal)
                var timerTotal: String = duracionTotal.seconds.toString() + "." + duracionTotal.nano.toString().substring(0, 3)
                var guardado = saveCotizacion(seguro, jsonEntrada, xmlCotizacion, timeRequest.toString(), soapResponse.obtenerNuevaEmisionResult, timerResponse, timerTotal)
                return seguro
            }

    }*/


    fun getXML() : String {




        val xmlCotizacion : String = "<oplCollection>" +
                "<Collection NoPoliza=\"7160154210\" NoNegocio=\"02886\" wpuid=\"bb832dad623e9cf6a4172d746f8f2af3\" wptoken=\"5bfb9c819296b6bb8da6410696e4fffd\">" +
                "<collectionData>" +
                "<type>C</type>" +
                "<userKey>PISM950928</userKey>" +
                "<name>Miguel Pineda</name>" +
                "<crypto>0</crypto>" +
                "<number>5477123412341234</number>" +
                "<bankcode>12</bankcode>" +
                "<expmonth>11</expmonth>" +
                "<expyear>2021</expyear>" +
                "</collectionData>" +
                "<insuranceData>" +
                "<akey>35863</akey>" +
                "<email>ihernandez@trigarante.com</email>" +
                "<PlazoPago>C</PlazoPago>" +
                "<currency>mxn</currency>" +
                "</insuranceData></Collection>" +
                "</oplCollection>"
        return xmlCotizacion
    }
}