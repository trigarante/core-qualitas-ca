package com.example.QualitasCore

import core.qualitas.wsdl.OplCollection
import core.qualitas.wsdl.OplCollectionResponse
import org.springframework.ws.client.core.support.WebServiceGatewaySupport
import org.springframework.ws.soap.client.core.SoapActionCallback

class Qualitas_Controller : WebServiceGatewaySupport() {

    fun cotizadorQualitas( xml : String?) : OplCollectionResponse{
       val request : OplCollection = OplCollection()
        request.xmlCollection = xml

        val respuesta =  getWebServiceTemplate()
                .marshalSendAndReceive("http://pagosqa.qualitas.com.mx/ws/wsCollection.php",
                request,SoapActionCallback("http://pagosqa.qualitas.com.mx/ws/wsCollection.php")) as OplCollectionResponse
        return respuesta
    }

}